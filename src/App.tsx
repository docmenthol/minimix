import * as React from 'react'
import { Sidebar, Menu, Container, Grid, Responsive, Button, Ref, Icon } from 'semantic-ui-react'
import produce from 'immer'
import { AppState, UpdaterFunctionParams, Action, ActionCallback } from './types'
import { actions, restoreState } from './store'
import Mixer from './Mixer'
import Output from './Output'
import Notebook from './Notebook'
import SaveModal from './SaveModal'
import ExportModal from './ExportModal'
import ImportModal from './ImportModal'
import * as lib from './lib'

type MinimixProps = {}
const MinimixContext = React.createContext({})

class App extends React.PureComponent<MinimixProps, {}> {
  public state: AppState = restoreState()
  private contentRef: any

  public constructor(props: MinimixProps) {
    super(props)
    lib.persistState(this.state)
  }

  public runAction = (type: Action, ...params: UpdaterFunctionParams) => {
    if (!actions[type]) {
      console.warn(`No action defined for ${type}.`)
      return
    }

    let f: ActionCallback = actions[type]
    let newState: AppState = produce(this.state, f(...params))
    this.setState(newState, () => lib.persistState(newState))
  }

  private handleWidthChange = (e: React.SyntheticEvent, data: any) => {
    if (data.width <= 1080 && this.state.sidebarVisible) {
      this.setState({ sidebarVisible: false, large: false })
    } else if (data.width > 1080 && !this.state.sidebarVisible) {
      this.setState({ sidebarVisible: true, large: true })
    }
  }

  private handleShowSidebar = (e: React.SyntheticEvent, data: any) =>
    this.setState({ sidebarVisible: true })

  private handleHideSidebar = (e: React.SyntheticEvent, data: any) =>
    this.setState({ sidebarVisible: false })

  private handleContentRef = (c: React.Component) => { this.contentRef = c }

  public render() {
    return (
      <>
        <Responsive fireOnMount onUpdate={this.handleWidthChange}>
          <Sidebar.Pushable style={{ minHeight: '100vh' }}>
            <Sidebar
              as={Menu}
              animation='overlay'
              icon='labeled'
              width='thin'
              onHide={this.handleHideSidebar}
              visible={this.state.sidebarVisible || this.state.large}
              target={this.contentRef}
              inverted
              vertical
            >
              <MinimixContext.Provider value={{ state: this.state, runAction: this.runAction }}>
                <Notebook />
              </MinimixContext.Provider>
            </Sidebar>

            <Sidebar.Pusher>
              <Ref innerRef={this.handleContentRef}>
                <Container style={{ marginTop: '2em' }}>
                  <Responsive maxWidth={1080}>
                    <Grid columns={1}>
                      <Grid.Row>
                        <Button basic animated onClick={this.handleShowSidebar} style={{ marginBottom: '1em', marginLeft: '1em' }}>
                          <Button.Content hidden>
                            <Icon name='chevron right' />
                          </Button.Content>
                          <Button.Content visible>
                            <Icon name='sidebar' />
                          </Button.Content>
                        </Button>
                      </Grid.Row>

                      <Grid.Row>
                        <Grid.Column>
                          <MinimixContext.Provider value={{ state: this.state, runAction: this.runAction }}>
                            <Mixer />
                          </MinimixContext.Provider>
                        </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Column>
                          <MinimixContext.Provider value={{ state: this.state }}>
                            <Output />
                          </MinimixContext.Provider>
                        </Grid.Column>
                      </Grid.Row>

                    </Grid>
                  </Responsive>

                  <Responsive minWidth={1081}>
                    <Grid columns={2}>
                      <Grid.Column width={10}>
                        <MinimixContext.Provider value={{ state: this.state, runAction: this.runAction }}>
                          <Mixer />
                        </MinimixContext.Provider>
                      </Grid.Column>
                      <Grid.Column width={6}>
                        <MinimixContext.Provider value={{ state: this.state }}>
                          <Output />
                        </MinimixContext.Provider>
                      </Grid.Column>
                    </Grid>
                  </Responsive>

                </Container>
              </Ref>
            </Sidebar.Pusher>
          </Sidebar.Pushable>
        </Responsive>

        <MinimixContext.Provider value={{ state: this.state, runAction: this.runAction }}>
          <SaveModal />
          <ExportModal />
          <ImportModal />
        </MinimixContext.Provider>
      </>
    )
  }
}

export default App
export { MinimixContext }
