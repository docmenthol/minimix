import * as React from 'react'
import { Table } from 'semantic-ui-react'
import { MinimixContext } from './App'
import { StateContext, CalculatedIngredient } from './types'

const IngredientRow = (props: { ingredient: CalculatedIngredient }) =>
  <Table.Row>
    <Table.Cell>{props.ingredient.name}</Table.Cell>
    <Table.Cell width={2} textAlign='right'>{props.ingredient.amount}</Table.Cell>
    <Table.Cell width={2} textAlign='right'>{props.ingredient.weight}</Table.Cell>
  </Table.Row>

const Output = () =>
  <Table celled inverted selectable striped unstackable>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell textAlign='center'>Ingredient</Table.HeaderCell>
        <Table.HeaderCell width={2} textAlign='center'>mL</Table.HeaderCell>
        <Table.HeaderCell width={2} textAlign='center'>g</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <MinimixContext.Consumer>
        {(context: StateContext) =>
          <>
            {context.state.calculated.map((ing: CalculatedIngredient, i: number) => <IngredientRow key={i} ingredient={ing} />)}
            <Table.Row>
              <Table.Cell textAlign='right'><strong>Total</strong></Table.Cell>
              <Table.Cell width={2} textAlign='right'>
                {!context.state.calculated.some(f => f.amount === '--')
                  ? Math.round(context.state.calculated.reduce((a, b) => a + (Number(b.amount) || 0), 0) * 100) / 100
                  : '--'
                }
              </Table.Cell>
              <Table.Cell width={2} textAlign='right'>
                {!context.state.calculated.some(f => f.amount === '--')
                  ? Math.round(context.state.calculated.reduce((a, b) => a + (Number(b.weight) || 0), 0) * 100) / 100
                  : '--'
                }
              </Table.Cell>
            </Table.Row>
          </>
        }
      </MinimixContext.Consumer>
    </Table.Body>
  </Table>


export default Output
