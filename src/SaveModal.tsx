import * as React from 'react'
import { MinimixContext } from './App'
import { FullContext } from './types'
import { Modal, Button, Input } from 'semantic-ui-react'
import * as lib from './lib'

const SaveModal = () => {
  const [name, setName] = React.useState('')

  return (
    <MinimixContext.Consumer>
      {(context: FullContext) =>
        <Modal open={context.state.saveOpen}>
          <Modal.Header>Save Recipe</Modal.Header>
          <Modal.Content>
            <Input
              value={name}
              placeholder='Enter recipe name.'
              onChange={(_: any, d: any) => setName(d.value)}
              fluid
            />
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={() =>
              context.runAction('SAVE_RECIPE', lib.makeRecipe(name, context.state.flavors, context.state.recipes))
            }>
              Save
            </Button>
            <Button onClick={() => context.runAction('TOGGLE_SAVE')}>Cancel</Button>
          </Modal.Actions>
        </Modal>
      }
    </MinimixContext.Consumer>
  )
}


export default SaveModal