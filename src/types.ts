type Flavor = {
  name: string,
  percent: string,
  vg: number,
  pg: number,
  id: number
}

type CalculatedIngredient = {
  name: string,
  amount: string | number,
  weight: string | number
}

type Recipe = {
  name: string,
  flavors: Array<Flavor>,
  id: number
}

type Parameters = {
  nic: number,
  target: number,
  amount: number,
  pg: number,
  vg: number
}

type NameDiff = {
  name: string
}

type PercentDiff = {
  percent: string
}

type FlavorDiff
  = NameDiff
  | PercentDiff

type ParamDiff = {
  nic?: number,
  target?: number,
  amount?: number,
  pg?: number,
  vg?: number
}

type AppState = {
  flavors: Array<Flavor>,
  recipes: Array<Recipe>,
  parameters: Parameters,
  calculated: Array<CalculatedIngredient>,
  sidebarVisible: boolean,
  large: boolean,
  saveOpen: boolean,
  exportOpen: boolean,
  importOpen: boolean
}

type ByIDParams = [number]
type FlavorDiffParams = [FlavorDiff, number]
type ParamDiffParams = [ParamDiff]
type FlavorArrayParams = [Array<Flavor>]
type VoidParams = []
type RecipeParams = [Recipe]

type UpdaterFunctionParams
  = ByIDParams
  | FlavorDiffParams
  | ParamDiffParams
  | FlavorArrayParams
  | VoidParams
  | RecipeParams

type StateContext = {
  state: AppState
}

type FullContext = {
  state: AppState,
  runAction: Dispatcher
}

type Action
  = 'ADD_FLAVOR'
  | 'UPDATE_PARAMETER'
  | 'UPDATE_FLAVOR_NAME'
  | 'UPDATE_FLAVOR_PERCENT'
  | 'REMOVE_FLAVOR'
  | 'CLEAR_FLAVORS'
  | 'SET_FLAVORS'
  | 'SAVE_RECIPE'
  | 'LOAD_RECIPE'
  | 'TOGGLE_SAVE'
  | 'TOGGLE_EXPORT'
  | 'TOGGLE_IMPORT'

interface Dispatcher {
  (type: Action, ...params: UpdaterFunctionParams): void
}

interface Updater {
  (draft: AppState): void
}

interface ActionCallback {
  (...params: UpdaterFunctionParams): Updater
}

export {
  Flavor,
  CalculatedIngredient,
  Recipe,
  Parameters,
  NameDiff,
  PercentDiff,
  FlavorDiff,
  ParamDiff,
  AppState,
  Dispatcher,
  Updater,
  UpdaterFunctionParams,
  StateContext,
  FullContext,
  Action,
  ActionCallback
}
