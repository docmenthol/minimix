import * as React from 'react'
import { Grid, Input, Button, InputOnChangeData, Responsive } from 'semantic-ui-react'
import { MinimixContext } from './App'
import { Flavor, FullContext } from './types'

type FlavorInputProps = {
  flavor: Flavor
}

const FlavorInput: React.SFC<FlavorInputProps> = (props: FlavorInputProps) =>
  <MinimixContext.Consumer>
    {(context: FullContext) =>
      <>
        <Responsive as={Grid.Row} maxWidth={991}>
          <Grid.Column width={11}>
            <Input
              placeholder='Unnamed Flavor'
              value={props.flavor.name}
              onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_FLAVOR_NAME', { name: d.value }, props.flavor.id)}
              fluid
            />
          </Grid.Column>
          <Grid.Column width={3} style={{ paddingLeft: 5, paddingRight: 20 }}>
            <Input
              placeholder='%'
              value={props.flavor.percent}
              onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_FLAVOR_PERCENT', { percent: d.value }, props.flavor.id)}
              fluid
            />
          </Grid.Column>
          <Grid.Column width={2} verticalAlign='middle' style={{ paddingLeft: 0 }}>
            <Button size='tiny' onClick={() => context.runAction('REMOVE_FLAVOR', props.flavor.id)} icon='remove' />
          </Grid.Column>
        </Responsive>

        <Responsive as={Grid.Row} minWidth={992}>
          <Grid.Column width={11}>
            <Input
              placeholder='Unnamed Flavor'
              value={props.flavor.name}
              onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_FLAVOR_NAME', { name: d.value }, props.flavor.id)}
              fluid
            />
          </Grid.Column>
          <Grid.Column width={3}>
            <Input
              label={{ basic: true, content: '%' }}
              labelPosition='right'
              value={props.flavor.percent}
              onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_FLAVOR_PERCENT', { percent: d.value }, props.flavor.id)}
              fluid
            />
          </Grid.Column>
          <Grid.Column width={2}>
            <Button onClick={() => context.runAction('REMOVE_FLAVOR', props.flavor.id)} icon='remove' />
          </Grid.Column>
        </Responsive>
      </>
    }
  </MinimixContext.Consumer>

const Mixer: React.SFC = () =>
  <MinimixContext.Consumer>
    {(context: FullContext) =>
      <Grid divided='vertically'>
        <Grid.Row>
          <Grid.Column>
            <Responsive maxWidth={991}>
              <Grid columns={3}>
                <Grid.Row>
                  <Grid.Column>
                    <Input
                      label='nic'
                      value={context.state.parameters.nic}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { nic: Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Input
                      label='tgt'
                      value={context.state.parameters.target}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { target: Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Input
                      label='amt'
                      value={context.state.parameters.amount}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { amount: Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid columns={2}>
                <Grid.Row>
                  <Grid.Column>
                    <Input
                      label='pg'
                      value={context.state.parameters.pg}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { pg: Number(d.value), vg: 100 - Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Input
                      label='vg'
                      value={context.state.parameters.vg}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { vg: Number(d.value), pg: 100 - Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Responsive>

            <Responsive minWidth={992}>
              <Grid columns={5}>
                <Grid.Row>
                  <Grid.Column>
                    <Input
                      label='nic'
                      value={context.state.parameters.nic}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { nic: Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Input
                      label='target'
                      value={context.state.parameters.target}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { target: Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Input
                      label='amt'
                      value={context.state.parameters.amount}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { amount: Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Input
                      label='pg'
                      value={context.state.parameters.pg}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { pg: Number(d.value), vg: 100 - Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                  <Grid.Column>
                    <Input
                      label='vg'
                      value={context.state.parameters.vg}
                      onChange={(_: React.ChangeEvent<HTMLInputElement>, d: InputOnChangeData) => context.runAction('UPDATE_PARAMETER', { vg: Number(d.value), pg: 100 - Number(d.value) })}
                      fluid
                    />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Responsive>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column>
            <Grid>
              {context.state.flavors.map((f: Flavor) => <FlavorInput key={f.id} flavor={f} />)}
              <Responsive as={Grid.Row} maxWidth={991}>
                <Grid.Column width={14}>
                  <Button
                    size='tiny'
                    icon='add'
                    onClick={() => context.runAction('ADD_FLAVOR')}
                  />
                </Grid.Column>
                <Grid.Column width={2} style={{ paddingLeft: 0 }}>
                  { context.state.flavors.length > 0 &&
                    <Button
                      size='tiny'
                      icon='trash alternate'
                      onClick={() => context.runAction('CLEAR_FLAVORS')}
                    />
                  }
                </Grid.Column>
              </Responsive>

              <Responsive as={Grid.Row} minWidth={992}>
                <Grid.Column width={14}>
                  <Button
                    icon='add'
                    onClick={() => context.runAction('ADD_FLAVOR')}
                  />
                </Grid.Column>
                <Grid.Column width={2}>
                  { context.state.flavors.length > 0 &&
                    <Button
                      icon='trash alternate'
                      onClick={() => context.runAction('CLEAR_FLAVORS')}
                    />
                  }
                </Grid.Column>
              </Responsive>
            </Grid>
          </Grid.Column>
        </Grid.Row>

      </Grid>
    }
  </MinimixContext.Consumer>

export default Mixer
