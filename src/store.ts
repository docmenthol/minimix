import { Flavor, ParamDiff, AppState, Recipe, NameDiff, PercentDiff } from './types'
import * as lib from './lib'

const emptyFlavor: Flavor = {
  name: '',
  percent: '0.0',
  pg: 100,
  vg: 0,
  id: -1
}

const initialState: AppState = {
  flavors: [],
  recipes: [],
  parameters: {
    nic: 100,
    target: 3,
    amount: 30,
    pg: 0,
    vg: 100
  },
  calculated: [],
  sidebarVisible: true,
  large: true,
  saveOpen: false,
  exportOpen: false,
  importOpen: false
}

const restoreState = (): AppState => {
  let cacheVal = window.localStorage.getItem('state')
  return cacheVal
    ? { ...initialState, ...JSON.parse(cacheVal) }
    : { ...initialState }
}

const actions = {
  'ADD_FLAVOR': () => (draft: AppState) => {
    let last = draft.flavors.slice(-1)[0] || { id: -1 }
    draft.flavors = [...draft.flavors, { ...emptyFlavor, id: last.id + 1 }]
    draft.calculated = lib.calculate(draft.parameters, draft.flavors)
  },
  'UPDATE_PARAMETER': (diff: ParamDiff) => (draft: AppState) => {
    let [ change ] = Object.entries(diff)
    if (!isNaN(Number(change[1]))) {
      draft.parameters = { ...draft.parameters, ...diff }
      draft.calculated = lib.calculate(draft.parameters, draft.flavors)
    }
  },
  'UPDATE_FLAVOR_NAME': (diff: NameDiff, id: number) => (draft: AppState) => {
    let index = draft.flavors.findIndex(f => f.id === id)
    draft.flavors[index].name = diff.name
    draft.calculated = lib.calculate(draft.parameters, draft.flavors)
  },
  'UPDATE_FLAVOR_PERCENT': (diff: PercentDiff, id: number) => (draft: AppState) => {
    let index = draft.flavors.findIndex(f => f.id === id)
    if (!isNaN(Number(diff.percent))) {
      draft.flavors[index].percent = diff.percent
      draft.calculated = lib.calculate(draft.parameters, draft.flavors)
    }
  },
  'REMOVE_FLAVOR': (id: number) => (draft: AppState) => {
    let index = draft.flavors.findIndex(f => f.id === id)
    draft.flavors = [
      ...draft.flavors.slice(0, index),
      ...draft.flavors.slice(index + 1)
    ]
    draft.calculated = lib.calculate(draft.parameters, draft.flavors)
  },
  'CLEAR_FLAVORS': () => (draft: AppState) => {
    draft.flavors = []
    draft.calculated = []
  },
  'SET_FLAVORS': (flavors: Array<Flavor>) => (draft: AppState) => {
    draft.flavors = flavors
    draft.calculated = lib.calculate(draft.parameters, draft.flavors)
  },
  'SAVE_RECIPE': (recipe: Recipe) => (draft: AppState) => {
    draft.recipes = [ ...draft.recipes, recipe ]
    draft.saveOpen = false
  },
  'LOAD_RECIPE': (id: number) => (draft: AppState) => {
    let index = draft.recipes.findIndex(r => r.id === id)
    draft.flavors = draft.recipes[index].flavors
    draft.calculated = lib.calculate(draft.parameters, draft.flavors)
  },
  'SET_RECIPES': (recipes: Array<Recipe>) => (draft: AppState) => {
    draft.recipes = recipes
  },
  'TOGGLE_SAVE': () => (draft: AppState) => {
    draft.saveOpen = !draft.saveOpen
  },
  'TOGGLE_EXPORT': () => (draft: AppState) => {
    draft.exportOpen = !draft.exportOpen
  },
  'TOGGLE_IMPORT': () => (draft: AppState) => {
    draft.importOpen = !draft.importOpen
  }
}

export { actions, restoreState }
