import { AppState, Flavor, Parameters, CalculatedIngredient, Recipe } from './types'

const keys = {
  top: ['flavors', 'recipes', 'calculated', 'parameters'],
  flavors: ['name', 'percent', 'pg', 'vg', 'id', 'weight'],
  params: ['nic', 'target', 'amount']
}

const persistState = (state: AppState) => {
  window.localStorage.setItem('state', JSON.stringify(state, [...keys.top, ...keys.flavors, ...keys.params]))
}

const round = (n: number, precision: number) => {
  let factor = Math.pow(10, precision)
  let tempNumber = n * factor
  let roundedTempNumber = Math.round(tempNumber)
  return roundedTempNumber / factor
}

const amt = (ta: number, p: number) => {
  const n = round(ta * (p / 100), 2)
  return isNaN(n) ? '--' : n
}

const calculate = (parameters: Parameters, flavors: Array<Flavor>) => {
  /* Calculate the recipe
   *  fp: total flavor percentage
   *  fa: total flavor amount in mL
   *  na: nicotine amount in mL
   *  ba: total pg/vg base amount in mL
   *  naw: nicotine amount in g
   *  pg: pg amount in mL
   *  vg: vg amount in mL
   *  pgw: pg amount in g
   *  vgw: vg amount in g
   *  a: { amount, weight }
   */

  const fp = flavors.reduce((a, b) => {
    let p = !isNaN(Number(b.percent)) ? Number(b.percent) : 0
    return a + p
  }, 0)
  const fa = parameters.amount * (fp / 100)
  const na = round(parameters.amount * (parameters.target / parameters.nic), 2)
  const ba = round(parameters.amount - (na + fa), 2)
  const naw = round(na * 1.23, 2)

  const pg = round((parameters.pg / 100) * ba, 2)
  const vg = round((parameters.vg / 100) * ba, 2)
  const pgw = round(pg * 1.04, 2)
  const vgw = round(vg * 1.26, 2)

  let rows: Array<CalculatedIngredient> = flavors
  .map(f => {
    const a = amt(parameters.amount, Number(f.percent))
    return { name: f.name || 'Unnamed Flavor', amount: a, weight: a }
  })

  // Add in the nicotine base and glycerin base amounts before returning
  rows.push({ name: 'Nic', amount: na, weight: naw })
  if (vg > 0) rows.push({ name: 'VG Base', amount: vg, weight: vgw })
  if (pg > 0) rows.push({ name: 'PG Base', amount: pg, weight: pgw })

  return rows
}

// const normalize = (parameters: Parameters, flavors: Array<Flavor>): Parameters => {
//   const { nic, amount, pg, vg } = parameters
//   let target = Math.min(parameters.target, nic)
//   const fp = flavors.reduce((a, b) => {
//     let p = !isNaN(Number(b.percent)) ? Number(b.percent) : 0
//     return a + p
//   }, 0)
//   const fa = amount * (fp / 100)
//   let na = round(amount * (target / nic), 2)
//   let ba = round(amount - (na + fa), 2)

//   if (ba < 0) {
//     // ba is the number of mL to remove
//     // let's just lower the maximum target nic level if we have to
//     target = Math.floor(target)
//     do {
//       target = target - 1
//       na = round(amount * (target / nic), 2)
//       ba = round(amount - (na + fa), 2)
//     } while (ba < 0)
//   }

//   return { nic, target, amount, pg, vg }
// }

const makeRecipe = (name: string, flavors: Array<Flavor>, recipes: Array<Recipe>) => {
  let id = recipes.length > 0
    ? recipes.slice(-1)[0].id + 1
    : 0
  return { name, flavors, id }
}

export {
  persistState,
  calculate,
  // normalize,
  makeRecipe
}