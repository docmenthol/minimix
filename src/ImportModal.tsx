import * as React from 'react'
import { MinimixContext } from './App'
import { FullContext } from './types'
import { Modal, Form, TextArea, Button } from 'semantic-ui-react'

const ImportModal = () => {
  const [recipes, setRecipes] = React.useState('')

  return (
    <MinimixContext.Consumer>
      {(context: FullContext) =>
        <Modal open={context.state.exportOpen}>
          <Modal.Header>Exported Recipes</Modal.Header>
          <Modal.Content>
            <Form>
              <TextArea rows={20} onChange={(_: any, d: any) => setRecipes(d.value)} />
            </Form>
          </Modal.Content>
          <Modal.Actions>
            <Button onClick={() => context.runAction('SET_RECIPES', recipes)}>
              Import
            </Button>
            <Button onClick={() => context.runAction('TOGGLE_IMPORT')}>Cancel</Button>
          </Modal.Actions>
        </Modal>
      }
    </MinimixContext.Consumer>
  )
}


export default ExportModal
