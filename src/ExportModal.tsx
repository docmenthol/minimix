import * as React from 'react'
import { MinimixContext } from './App'
import { FullContext } from './types'
import { Modal, Form, TextArea, Button } from 'semantic-ui-react'

const ExportModal = () =>
  <MinimixContext.Consumer>
    {(context: FullContext) =>
      <Modal open={context.state.exportOpen}>
        <Modal.Header>Exported Recipes</Modal.Header>
        <Modal.Content>
          <Form>
            <TextArea value={JSON.stringify(context.state.recipes, null, 2)} rows={20} />
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={() => context.runAction('TOGGLE_EXPORT')}>Close</Button>
        </Modal.Actions>
      </Modal>
    }
  </MinimixContext.Consumer>


export default ExportModal
