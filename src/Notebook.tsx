import * as React from 'react'
import { Menu, Icon } from 'semantic-ui-react'
import { MinimixContext } from './App'
import { Recipe, FullContext } from './types'

const Notebook = () =>
  <MinimixContext.Consumer>
    {(context: FullContext) =>
      <>
        <Menu.Item as='div'>
          <Icon name='tint' />
          Recipes
          </Menu.Item>
        {context.state.recipes.map((r: Recipe) =>
          <Menu.Item
            key={r.id}
            as='a'
            onClick={() => context.runAction('LOAD_RECIPE', r.id)}
          >
            {r.name}
          </Menu.Item>)
        }
        <Menu.Item as='a' onClick={() => context.runAction('TOGGLE_SAVE')}>
          <Icon name='add circle' />
        </Menu.Item>
        <Menu.Item as='a' onClick={() => context.runAction('TOGGLE_EXPORT')}>
          <Icon name='download' />
        </Menu.Item>
        <Menu.Item as='a' onClick={() => context.runAction('TOGGLE_IMPORT')}>
          <Icon name='upload' />
        </Menu.Item>
      </>
    }
  </MinimixContext.Consumer>


export default Notebook
